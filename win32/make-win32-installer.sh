#!/usr/bin bash
set -e

WIX_URI="https://github.com/wixtoolset/wix3/releases/download/wix3112rtm/wix311-binaries.zip"
WIXDIR="/tmp/wix_toolset"
TMPDIR=/tmp/win32-build
ICON_URL="${CI_PROJECT_URL}/raw/${CI_BUILD_REF_NAME}/${ICON_PATH}"
DATA_DIR="$( cd "$(dirname "$0")" ; pwd -P )"/data
ICO_PATH="${DATA_DIR}/${CI_PROJECT_NAME}.ico"
MAKEPKG_FILE="/tmp/${CI_PROJECT_NAME}"-git.pkg.tar.xz

if [ "${MSYSTEM}" = "MINGW32" ]; then
  BITNESS=32
  PLATFORM="x86"
  ARCH="i686"
else
  ARCH="x86_64"
  BITNESS=64
  PLATFORM="x64"
fi

echo "APP_NAME        = ${APP_NAME}"
echo "BIN_NAME        = ${BIN_NAME}"
echo "MANUFACTURER    = ${MANUFACTURER}"
echo "MESON ARGS      = ${MESON_ARGS}"
echo "ARCH            = ${ARCH}"
echo "MSYSTEM         = ${MSYSTEM}"
echo "VERSION         = ${VERSION}"
echo "BUNDLE          = ${MSI_BUNDLE}"
echo "GIT_REPO        = ${CI_PROJECT_URL}/${CI_BUILD_REF_NAME}"

echo "Creating win32 chroot system ${TMPDIR}"
pacman --noconfirm -S --needed base-devel binutils unzip wget "mingw-w64-${ARCH}-pkg-config" "mingw-w64-${ARCH}-imagemagick" "mingw-w64-${ARCH}-librsvg"
# Build
echo "Building the application"
cp "${DATA_DIR}/PKGBUILD" /tmp
pushd /tmp> /dev/null
  sed -i "s|@VERSION@|${VERSION}|g;s|@APP_NAME@|${APP_NAME}|g;s|@MESON_ARGS@|${MESON_ARGS}|g;s|@ARCH@|${ARCH}|g;s|@GIT_REPO@|${CI_PROJECT_URL}.git|g;s|@GIT_BRANCH@|${CI_BUILD_REF_NAME}|g" PKGBUILD
  MINGW_INSTALLS="mingw${BITNESS}" PKGDEST=/tmp PKGEXT=".pkg.tar.gz" makepkg-mingw -sCLf --noconfirm
popd > /dev/null

[ -d "${TMPDIR}" ] && rm -rf "${TMPDIR}"
mkdir -p "${TMPDIR}"
pushd "${TMPDIR}"> /dev/null
  mkdir -p var/lib/pacman
  mkdir -p var/log
  mkdir -p tmp

  pacman --noconfirm --needed -Syu --root "${TMPDIR}"
  pacman -S filesystem bash pacman --needed --noconfirm --root "${TMPDIR}"
  pacman --noconfirm --needed --root "${TMPDIR}" -U /tmp/*.pkg.tar.gz
popd > /dev/null
ls -la "${TMPDIR}"

[ -f ${MAKEPKG_FILE} ] && rm -f ${MAKEPKG_FILE}
echo "Creating win32 installer /tmp/${MSI_BUNDLE}"
[ -f /tmp/${MSI_BUNDLE} ] && rm -f /tmp/${MSI_BUNDLE}

sed -i "s|@VERSION@|${VERSION}|g;s|@APP_NAME@|${APP_NAME}|g;s|@MANUFACTURER@|${MANUFACTURER}|g" "${DATA_DIR}/defines.wxi"
sed -i "s|@APP_NAME@|${APP_NAME}|g;s|@PROJECT_NAME@|${CI_PROJECT_NAME}|g;s|@BIN_NAME@|${BIN_NAME}|g" "${DATA_DIR}/app.wxs"


echo "Downloading Wix Toolset"
[ -d "${WIXDIR}" ] && rm -rf "${WIXDIR}"
mkdir -p "${WIXDIR}"
wget -O /tmp/wix_toolset.zip ${WIX_URI}
unzip -d ${WIXDIR} /tmp/wix_toolset.zip

echo "Preparing bundle icon"
wget -O /tmp/${CI_PROJECT_NAME}.svg ${ICON_URL}
rsvg-convert -f png -o /tmp/${CI_PROJECT_NAME}.png /tmp/${CI_PROJECT_NAME}.svg
convert /tmp/${CI_PROJECT_NAME}.png ${ICO_PATH}

mkdir -p "installer/SourceDir"
cp -R "${TMPDIR}/mingw${BITNESS}" "installer/SourceDir"

rm -rf installer/SourceDir/share/help
rm -rf installer/SourceDir/share/gtk-doc
rm -rf installer/SourceDir/share/doc
rm -rf installer/SourceDir/share/pkgconfig
rm -rf installer/SourceDir/share/bash-completion
rm -rf installer/SourceDir/share/appdata
rm -rf installer/SourceDir/share/man
rm -rf installer/SourceDir/lib/peas-demo
rm -rf installer/SourceDir/bin/gtk3-demo*.exe


# strip the binaries to reduce the size
find installer/SourceDir -name *.dll | xargs strip

pushd "installer" > /dev/null
"${WIXDIR}/heat.exe" dir SourceDir -gg -dr INSTALLDIR -cg binaries -sfrag -sreg -srd -suid -template fragment -out binaries.wxs
"${WIXDIR}/candle.exe" -arch ${PLATFORM} "${DATA_DIR}"/app.wxs binaries.wxs
"${WIXDIR}/light.exe" -ext WixUtilExtension -ext WixUIExtension app.wixobj binaries.wixobj -o "/tmp/${MSI_BUNDLE}"
popd

mv "/tmp/${MSI_BUNDLE}" "$( cd "$(dirname "$0")" ; pwd -P )/../../${MSI_BUNDLE}"
